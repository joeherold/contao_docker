#!/bin/bash

cd "$(dirname "$0")";
cd ../..;

# mkdir -p ./app/contao/web/;
# docker-compose run --rm --user=\"application\" app curl https://download.contao.org/contao-manager/stable/contao-manager.phar -o /app/contao/web/contao-manager.phar.php
docker-compose run --rm app mkdir -p /app/contao/web/; 
docker-compose run --rm app curl https://download.contao.org/contao-manager/stable/contao-manager.phar -o /app/contao/web/contao-manager.phar.php;
osascript -e 'display notification "contao-manager.phar has been installted into TL_ROOT/contao/web/ folder" with title "contao-manager.phar" sound name "default"' 
