#!/bin/bash

cd "$(dirname "$0")";
cd ../..;

sh ./bin/scripts/helper/prepare_composer_install.sh
docker-compose up -d;
docker-compose run --name="Contao_4_installer" --user="application" --no-deps -e COMPOSER_ALLOW_SUPERUSER=1 --rm app composer create-project contao/managed-edition contao '4.6.*';
docker-compose run --name="Contao_4_installer" --user="application" --no-deps -e COMPOSER_ALLOW_SUPERUSER=1 --rm app mkdir -p /app/contao/web/; 
docker-compose run --name="Contao_4_installer" --user="application" --no-deps -e COMPOSER_ALLOW_SUPERUSER=1 --rm app curl https://download.contao.org/contao-manager/stable/contao-manager.phar -o /app/contao/web/contao-manager.phar.php;

sh ./bin/scripts/helper/recopy_tmp_files.sh

sh ./bin/scripts/helper/install_theme_structure.sh

osascript -e 'display notification "Contao 4.6.* installed" with title "Contao Composer installation" sound name "default"' 
