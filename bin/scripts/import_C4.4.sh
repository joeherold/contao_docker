#!/bin/bash

cd "$(dirname "$0")";
cd ../..;

rm -R ./app/contao;
mkdir -p ./app/contao;

cd ./app/contao;
# cp ./import/contao_4.4.LTS.tar.gz ./app/contao;
curl -L https://bitbucket.org/joeherold/contao_docker/downloads/contao_4.4.LTS.tar.gz -o contao_4.4.LTS.tar.gz
# mv ./app/contao_4.4.LTS.tar.gz ./app/contao.tar.gz;

tar -zxf contao_4.4.LTS.tar.gz;

cd ..;
cd ..;

rm -R ./app/contao/contao_4.4.LTS.tar.gz;
docker-compose up -d;
docker exec -i $(docker-compose ps -q mysql) env MYSQL_PWD=root mysql -uroot contao < "app/contao/db.backup.sql"

docker-compose run --workdir="/app/contao" --name="Contao_4_installer" --user="application" --no-deps -e COMPOSER_ALLOW_SUPERUSER=1 --rm app composer install;

osascript -e 'display notification "Contao 4.4 LTS imported" with title "Contao Composer import" sound name "default"' 
