#!/bin/bash

cd "$(dirname "$0")";
cd ../../..;

mkdir -p ./app/tmp;
mkdir -p ./app/tmp/app;
mkdir -p ./app/tmp/files;
mkdir -p ./app/tmp/system/config;
mkdir -p ./app/tmp/system/modules;
mkdir -p ./app/tmp/templates;
mkdir -p ./app/contao;
mkdir -p ./app/contao/files;

cp -R ./app/contao/composer.json ./app/tmp/composer.json;
cp -R ./app/contao/composer.lock ./app/tmp/composer.lock;


cp -R ./app/contao/app/* ./app/tmp/app/;
cp -R ./app/contao/files/* ./app/tmp/files/;
cp -R ./app/contao/system/config/* ./app/tmp/system/config/;
cp -R ./app/contao/system/modules/* ./app/tmp/system/modules/;

rm -R ./app/contao/;