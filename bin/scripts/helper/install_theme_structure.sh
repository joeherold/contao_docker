#!/bin/bash

cd "$(dirname "$0")";
cd ../../..;

mkdir -p ./app/contao/files/layout/assets/fonts; 
mkdir -p ./app/contao/files/layout/assets/js;
touch    ./app/contao/files/layout/assets/js/main.js; 
mkdir -p ./app/contao/files/layout/assets/css;
mkdir -p ./app/contao/files/layout/assets/less;
mkdir -p ./app/contao/files/layout/assets/sass;
touch    ./app/contao/files/layout/assets/less/main_less.less;
touch    ./app/contao/files/layout/assets/sass/main_sass.scss;

# mkdir -p ./app/contao/files/layout/dist/js;
# mkdir -p ./app/contao/files/layout/dist/css;

mkdir -p ./app/contao/files/layout/images;

cp ./docker/csswatcher/gulp.yaml ./app/contao/files/;

osascript -e 'display notification "Base Theme-Structure created" with title "Theme Structure" sound name "default"' 