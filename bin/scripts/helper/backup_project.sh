#!/bin/bash

cd "$(dirname "$0")";
cd ../../..;

year=$(date +'%Y');
month=$(date +'%m');
day=$(date +'%d');

hour=$(date +'%H');
minute=$(date +'%M');
second=$(date +'%S');
# echo $year;
# echo $month;
# echo $day;
# mkdir -p "backups/app/${year}/${month}/${day}/";
tstamp=$(date +'%s')

rm -R ./app/tmp;
mkdir -p ./app/tmp;
mkdir -p ./app/tmp/app;
mkdir -p ./app/tmp/assets;
mkdir -p ./app/tmp/files;
mkdir -p ./app/tmp/web;
mkdir -p ./app/tmp/system;
mkdir -p ./app/tmp/templates;
mkdir -p ./app/tmp/var/cache/;
mkdir -p ./app/tmp/var/logs/;
mkdir -p ./app/tmp/vendor/;

cp -R ./app/contao/app/* ./app/tmp/app/;
cp -R ./app/contao/assets/* ./app/tmp/assets/;
cp -R ./app/contao/files/* ./app/tmp/files/;
cp -R ./app/contao/web/* ./app/tmp/web/;
cp -R ./app/contao/system/* ./app/tmp/system/;
cp -R ./app/contao/templates/* ./app/tmp/templates/;
cp -R ./app/contao/var/logs/* ./app/tmp/var/logs/;




cp ./app/contao/.gitignore ./app/tmp/;
cp ./app/contao/.travis.yml ./app/tmp/;
cp ./app/contao/composer.json ./app/tmp/;
cp ./app/contao/composer.lock ./app/tmp/;
cp ./app/contao/README.md ./app/tmp/;
docker-compose up -d;
# docker-compose exec mysql usr/bin/mysqldump -u root --password=root contao > "app/tmp/db.backup.sql";
docker-compose exec mysql env MYSQL_PWD=root usr/bin/mysqldump -u root contao > "app/tmp/db.backup.sql"
cd "./app/tmp";
tar -czf  "../../backups/app/${year}_${month}_${day}___${hour}_${minute}_${second}.contao.tar.gz"  ".";
cd ..;
cd ..;
sh ./bin/scripts/helper/backup_database.sh

rm -R ./app/tmp;
# tar -czvf "./backups/app/${year}/${month}/${day}/app.${tstamp}.tar.gz" "./app/tmp"    
osascript -e 'display notification "Contao Backup done" with title "Backup finished" sound name "default"' 