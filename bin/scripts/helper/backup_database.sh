#!/bin/bash

cd "$(dirname "$0")";
cd ../../..;

year=$(date +'%Y');
month=$(date +'%m');
day=$(date +'%d');

hour=$(date +'%H');
minute=$(date +'%M');
second=$(date +'%S');

# docker-compose run mysql mysqld --sql_mode="";
docker-compose up -d;
docker-compose exec mysql env MYSQL_PWD=root usr/bin/mysqldump -u root contao > "backups/database/${year}_${month}_${day}___${hour}_${minute}_${second}.sql"
# docker-compose exec mysql usr/bin/mysqldump --login-path=local contao > "backups/database/${year}_${month}_${day}___${hour}_${minute}_${second}.sql"

tar -czf "backups/database/${year}_${month}_${day}___${hour}_${minute}_${second}.sql.tar.gz" "backups/database/${year}_${month}_${day}___${hour}_${minute}_${second}.sql"
mv  "backups/database/${year}_${month}_${day}___${hour}_${minute}_${second}.sql" "backups/database/latest.sql";
# rm -R "backups/database/${year}_${month}_${day}___${hour}_${minute}_${second}.sql";
# docker-compose stop mysql;
osascript -e 'display notification "Database Backup done" with title "Backup finished" sound name "default"' 