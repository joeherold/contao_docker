#!/bin/bash

cd "$(dirname "$0")";
cd ../../..;
docker-compose up -d;
docker exec -i $(docker-compose ps -q mysql) env MYSQL_PWD=root mysql -uroot contao < "app/contao/db.backup.sql"

osascript -e 'display notification "DB Import completed" with title "app/contao/db.backup.sql" sound name "default"' 
