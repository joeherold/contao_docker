#!/bin/bash

cd "$(dirname "$0")";
cd ../../..;

cp -R ./app/tmp/app/* ./app/contao/app/ ;
cp -R ./app/tmp/files/* ./app/contao/files/;
cp -R ./app/tmp/system/config/* ./app/contao/system/config/;
cp -R ./app/tmp/system/modules/* ./app/contao/system/modules/;

tar -czvf ./backups/app/archive.tar.gz ./app/tmp/*;

rm -R ./app/tmp/;