#!/bin/bash

cd "$(dirname "$0")";
cd ../..;

docker-compose up -d;
osascript -e 'display notification "Development Container is UP" with title "Container UP" sound name "default"';