#!/bin/bash

cd "$(dirname "$0")";
cd ../..;

docker-compose down;
osascript -e 'display notification "Development Container is DOWN" with title "Container DOWN" sound name "default"';