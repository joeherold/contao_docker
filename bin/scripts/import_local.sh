#!/bin/bash

cd "$(dirname "$0")";
cd ../..;

rm -R ./app/contao;
mkdir -p ./app/contao;

cp ./app/import.tar.gz ./app/contao;
cd ./app/contao;
tar -zxf import.tar.gz;
cd ..;
cd ..;

rm -R ./app/contao/import.tar.gz;
rm -R ./app/contao/vendor; 
rm -R ./app/contao/var;
docker-compose up -d;
docker exec -i $(docker-compose ps -q mysql) env MYSQL_PWD=root mysql -uroot contao < "app/contao/db.backup.sql"

docker-compose run --workdir="/app/contao" --name="Contao_4_installer" --user="application" --no-deps -e COMPOSER_ALLOW_SUPERUSER=1 --rm app composer install;

osascript -e 'display notification "Contao 4.4 LTS imported" with title "Contao Composer import" sound name "default"' 
