'use strict';


let notification = require("./notification.helper");

function errHandler(err) {
    notification.error(err);
    // notification('[ERROR] ' + err.toString(), "error");
    if (process.env.NODE_ENV !== "production") {
        if(err) console.error("error: ", err);
    }
}

module.exports = errHandler;