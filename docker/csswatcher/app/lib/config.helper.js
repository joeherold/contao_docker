'use strict';

const yaml = require('js-yaml');
const fs = require('fs');
const _ = require("lodash");
const path = require("path");
const deepExtend = require('deep-extend');
const notificationsHelper = require("./notification.helper");

let TL_FILES;
let GULP_YAML_SETTINGS_PATH = "";
let TL_ROOT;
if (process.env.NODE_ENV == "production") {
    TL_ROOT = "/sync_root/contao/";
    TL_FILES = "/sync_root/contao/files/";
    GULP_YAML_SETTINGS_PATH = "/sync_root/contao/files/gulp.yaml";

} else {
    TL_ROOT = path.resolve("../../../app/contao/");
    TL_FILES = path.resolve("../../../app/contao/files/");
    GULP_YAML_SETTINGS_PATH = path.resolve("../../../app/contao/files/gulp.yaml");
}


var paths = {
    src: {
        scss: TL_FILES + "**/*.{sass,scss}",
        less: TL_FILES + "**/*.{less}",
        scripts: TL_FILES + "**/*.{js}"
    },
    dist: {
        styles: TL_FILES + "dist/"
    }
}



let YAML_CONFIG_DEFAULT = {
    config: {
        scss: {
            sources: [
                "/files/**/*.{sass,scss}"
            ],
            destination: "files/dist/scss",
            base: "",
            outputStyle: "compressed",
            createSourceMaps: true,
            precision: 5,
            sourceComments: false,
            watcher: true,
            advanced: {
                rename: {
                    enabled: false,
                    settings: null
                }
            }
        },
        less: {
            sources: [
                "/files/**/*.{less}"
            ],
            destination: "files/dist/scss",
            base: "",
            outputStyle: "compressed",
            createSourceMaps: true,
            precision: 5,
            sourceComments: false,
            watcher: true,
            advanced: {
                rename: {
                    enabled: false,
                    settings: null
                }
            }
        },
        cleanCSS: {
            enabled: false,
            settings: null
        },
        autoprefixer: {
            enabled: false,
            settings: null
        },
        javascript: {
            sources: [
                "/files/**/*.{js}"
            ],
            destination: "files/dist/less",
            base: "",
            compressed: true,
            concat: false,
            concatFileName: "all.js",
            concatFolderName: "combined",
            watcher: true
        }
    }
};

let YAML_CONFIG = { ...YAML_CONFIG_DEFAULT };

try {
    let YAML_CONFIG_LOADED = yaml.safeLoad(fs.readFileSync(GULP_YAML_SETTINGS_PATH, 'utf8'));
    if (YAML_CONFIG_LOADED && YAML_CONFIG_LOADED['config']) {
        deepExtend(YAML_CONFIG, YAML_CONFIG_LOADED);
    }

} catch (e) {
    // no modification of default config
    notificationsHelper.error(e);

}

let exporter = {
    TL_ROOT,
    TL_FILES,
    GULP_YAML_SETTINGS_PATH,
    GULP_SETTINGS: YAML_CONFIG['config'],
    getFullPathsOnEnvironment: function (inputData, callerInfo) {
        let retData = null;
        if (Array.isArray(inputData)) {
            // an array of paths is passed 
            retData = inputData.map((value, index, arr) => path.join(TL_ROOT, _.trimEnd(value, "/")));
        } else if (typeof inputData === "string") {
            retData = path.join(TL_ROOT, _.trimEnd(inputData, "/"));
        } else {
            retData = inputData;
        }

        if (process.env.NODE_ENV !== "production") {
            if (callerInfo) {
                //console.log("getFullPathsOnEnvironment callerInfo: ", callerInfo);
            }
            //console.log("getFullPathsOnEnvironment:", retData); 
        }

        return retData;
    }
};

if (process.env.NODE_ENV !== "production") {

    //console.log("YAML CONFIGURATION: ", exporter);
}


module.exports = exporter;