'use strict';
const axios = require('axios');
const callerID = require('caller-id');

function notification(message, status = "success") {
    var url = 'http://nodesync:8080/messages/scss?api_key=DEMO_KEY';
    if (process.env.NODE_ENV !== "production") {
        url = 'http://localhost:8080/messages/scss?api_key=DEMO_KEY';
    }
    if(message){
        if (typeof message === "string") {

        } else if (typeof message.toString === "function") {
            message = message.toString() + "";
        } else if (typeof message === "undefined") {
            message = "EMPTY MESSAGE...";
        }
    } else {
        message = "EMPTY MESSAGE...";
        ////console.log.log(callerID.getData());
    }
    

    let prepender = "";
    switch (status) {
        case "success":
            prepender = "[SUCCESS]";
            break;
        case "info":
            prepender = "[INFO]";
            break;
        case "error":
            prepender = "[ERROR]";
            break;
        default:
            break;
    }

    if (process.env.NODE_ENV !== "production") {
        console.log("[NOTIFICATION]: ", prepender + " " + message);
        // //console.log.log("Called by: ", callerID.getData());
    }
    axios.post(url, {
        status: status,
        message: prepender + " " + message
    })
        .then(response => {
            // //console.log.log(response.data.url);
            // //console.log.log(response.data.explanation);
        })
        .catch(error => {
            // //console.log.log(error);
        });

}





module.exports = {
    notification: notification,
    error: function (errMessage) {
        notification(errMessage, "error");
    },
    success: function (successMessage) {
        notification(successMessage, "success");
    },
    info: function (infoMessage) {
        notification(infoMessage, "info");
    }
}