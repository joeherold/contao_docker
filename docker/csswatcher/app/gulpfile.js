'use strict';

/**
 * NODE MODULES IMPORT
 */

const yaml = require('js-yaml');
const fs = require('fs');
const path = require('path');
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
// var imagemin = require('gulp-imagemin');
// var pngcrush = require('imagemin-pngcrush');
var include = require('gulp-include');
var autoprefixer = require('gulp-autoprefixer');
var pixrem = require('gulp-pixrem');
var notify = require('gulp-notify');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var jslint = require('gulp-jslint');
// var csscomb       = require('gulp-csscomb');
var gutil = require('gulp-util');
//var wait = require("gulp-wait");
var rename = require('gulp-rename');
var watch = require('gulp-watch');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var jsMinify = require('gulp-minify');

// console.log = function(){

// }
// console.info = function(){

// }

/** local imports */
let notification = require("./lib/notification.helper").notification;
let notificationHelper = require("./lib/notification.helper");
let errHandler = require('./lib/error.helper')
let configHelper = require('./lib/config.helper');
let GULP_SETTINGS = require('./lib/config.helper').GULP_SETTINGS;


let AUTOPREFIXER_ENABLED = (GULP_SETTINGS.autoprefixer.enabled === true);
let AUTOPREFIXER_SETTINGS = { ...{ browsers: ['last 8 versions'], cascade: false }, ...GULP_SETTINGS.autoprefixer.settings };

let SCSS_SASS_SETTINGS = {
    outputStyle: GULP_SETTINGS.scss.outputStyle,
    precision: GULP_SETTINGS.scss.precision,
    sourceComments: GULP_SETTINGS.scss.sourceComments,
}
let LESS_SASS_SETTINGS = {
    outputStyle: GULP_SETTINGS.less.outputStyle,
    precision: GULP_SETTINGS.less.precision,
    sourceComments: GULP_SETTINGS.less.sourceComments,
}

SCSS_SASS_SETTINGS = { ...{ outputStyle: 'compressed' }, ...SCSS_SASS_SETTINGS };
LESS_SASS_SETTINGS = { ...{ outputStyle: 'compressed' }, ...LESS_SASS_SETTINGS };

let SCSS_RENAME_ENABLED = (GULP_SETTINGS.scss.advanced.rename.enabled === true);
let SCSS_RENAME = { ...{ suffix: "_scss" }, ...GULP_SETTINGS.scss.advanced.rename.settings };

let LESS_RENAME_ENABLED = (GULP_SETTINGS.less.advanced.rename.enabled === true);
let LESS_RENAME = { ...{ suffix: "_less" }, ...GULP_SETTINGS.less.advanced.rename.settings };

let CLEANCSS_ENABLED = (GULP_SETTINGS.cleanCSS.enabled === true);
let CLEANCSS_SETTINGS = { ...{}, ...GULP_SETTINGS.cleanCSS.settings };

let SCSS_SOURCEMAPS_ENABLED = (GULP_SETTINGS.scss.createSourceMaps === true);
let LESS_SOURCEMAPS_ENABLED = (GULP_SETTINGS.less.createSourceMaps === true);
let JS_SOURCEMAPS_ENABLED = (GULP_SETTINGS.javascript.createSourceMaps === true);



/**
 * DEFAULT SCSS SASS TASK
 */
gulp.task('scss', function () {

    var stream = gulp.src(
        configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.scss.sources, "scss gulp.src"),
        { base: configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.scss.base) }
    );

    // add error stop prevents
    stream = stream.pipe(plumber());

    if (SCSS_SOURCEMAPS_ENABLED) {
        stream = stream.pipe(sourcemaps.init())
            .on('error', errHandler);
    }

    stream = stream.pipe(sass({ ...SCSS_SASS_SETTINGS }))
        .on('error', errHandler);

    if (AUTOPREFIXER_ENABLED) {
        stream = stream.pipe(autoprefixer({ ...AUTOPREFIXER_SETTINGS }))
            .on('error', errHandler);
    }
    if (CLEANCSS_ENABLED) {
        stream = stream.pipe(cleanCSS({ ...CLEANCSS_SETTINGS }))
            .on('error', errHandler);
    }
    if (SCSS_RENAME_ENABLED) {
        stream = stream.pipe(rename({ ...SCSS_RENAME }))
            .on('error', errHandler);
    }

    if (SCSS_SOURCEMAPS_ENABLED) {
        stream = stream.pipe(sourcemaps.write('maps'))
            .on('error', errHandler);
    }

    stream = stream.pipe(
        gulp.dest(
            // GULP_SETTINGS.scss.destination
            configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.scss.destination, "scss gulp.dest")
        )
    )
    stream.on('error', errHandler);
    // stream.on('end', () => { notificationHelper.info("end") });
    // stream.on('finish', () => { notificationHelper.info("finish sass/scss") });


})

/** 
 * ALIAS for SASS
 */
gulp.task('sass', ['scss']);

/**
 * DEFAULT LESS TASK
 */
gulp.task('less', function () {
    // gutil.log(GULP_SETTINGS.less);
    var stream = gulp.src(
        configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.less.sources, "less gulp.src"),
        { base: configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.less.base) }
    );

    // add error stop prevents
    stream = stream.pipe(plumber());

    if (LESS_SOURCEMAPS_ENABLED) {
        stream = stream.pipe(sourcemaps.init())
            .on('error', errHandler);
    }

    stream = stream.pipe(less())
        .on('error', errHandler);

    if (AUTOPREFIXER_ENABLED) {
        stream = stream.pipe(autoprefixer({ ...AUTOPREFIXER_SETTINGS }))
            .on('error', errHandler);
    }
    if (CLEANCSS_ENABLED) {
        stream = stream.pipe(cleanCSS({ ...CLEANCSS_SETTINGS }))
            .on('error', errHandler);
    }
    if (LESS_RENAME_ENABLED) {
        stream = stream.pipe(rename({ ...LESS_RENAME }))
            .on('error', errHandler);
    }

    if (LESS_SOURCEMAPS_ENABLED) {
        stream = stream.pipe(sourcemaps.write('maps'))
            .on('error', errHandler);
    }

    stream = stream.pipe(
        gulp.dest(
            // GULP_SETTINGS.scss.destination
            configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.less.destination, "less gulp.dest")
        )
    )
    stream.on('error', errHandler);
    // stream.on('end', () => { notificationHelper.info("end") });
    // stream.on('finish', () => { notificationHelper.info("finish less") });

})

gulp.task('scripts', function () {
    var stream = gulp.src(configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.javascript.sources),
        { base: configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.javascript.base) }
    );

    // add error stop prevents
    stream = stream.pipe(plumber());

    stream = stream.pipe(include())
        .on('error', errHandler);

    if (JS_SOURCEMAPS_ENABLED) {
        stream = stream.pipe(sourcemaps.init())
            .on('error', errHandler);
    }

    // this enables to use ES6, ES7 Features for writing Javascript, like classes, spread operator and so on.
    stream = stream.pipe(babel({
        presets: ['@babel/env']
    }));

    if (GULP_SETTINGS.javascript.compressed === true) {
        stream = stream.pipe(jsMinify({
            ext: {
                src: '.js',
                min: '.min.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
            .on('error', errHandler);
    }

    if (JS_SOURCEMAPS_ENABLED) {
        stream = stream.pipe(sourcemaps.write("maps", {
            // includeContent: false
        }))
            .on('error', errHandler);
    }

    stream = stream.pipe(
        gulp.dest(configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.javascript.destination))
    );

    stream.on('error', errHandler)
    // stream.on('finish', () => { notificationHelper.info("finish scripts") });


    if (GULP_SETTINGS.javascript.concat === true) {

        var streamAll = gulp.src(configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.javascript.sources),
            { base: configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.javascript.base) }
        );

        // add error stop prevents
        streamAll = streamAll.pipe(plumber());

        streamAll = streamAll.pipe(include())
            .on('error', errHandler);

        streamAll = streamAll.pipe(sourcemaps.init())
            .on('error', errHandler);

        streamAll = streamAll.pipe(babel({
            presets: ['@babel/env']
        }));


        var concatFileName = "all.js";
        var concatFolderName = "combined";
        if (GULP_SETTINGS.javascript.concatFileName && GULP_SETTINGS.javascript.concatFileName !== "") {
            concatFileName = "" + GULP_SETTINGS.javascript.concatFileName;
        }
        if (GULP_SETTINGS.javascript.concatFolderName && GULP_SETTINGS.javascript.concatFolderName !== "") {
            concatFolderName = "/" + GULP_SETTINGS.javascript.concatFolderName;
        }

        streamAll = streamAll.pipe(concat(concatFileName, { newLine: ';' }))
            .on('error', errHandler);

        streamAll = streamAll.pipe(jsMinify({
            ext: {
                src: '.js',
                min: '.min.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js', '.min.js']
        }))
            .on('error', errHandler);

        streamAll = streamAll.pipe(sourcemaps.write("maps"))
            .on('error', errHandler);


        streamAll = streamAll.pipe(
            // gulp.dest(
            //     concatFolderName
            // )
            gulp.dest(configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.javascript.destination) + concatFolderName)
        )
        // .on('error', errHandler);
        streamAll.on('error', errHandler)
        // streamAll.on('finish', () => { notificationHelper.info("finish concat scripts") });
    }

});

gulp.task('watch_scss', ['scss'], function () {
    return watch(
        configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.scss.sources, "watch_scss src"),
        { debounceDelay: 2000 }, function () {
            gulp.start("scss");
        }
    );
});

gulp.task('watch_less', ['less'], function () {
    return watch(
        configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.less.sources, "watch_less src"),
        { debounceDelay: 2000 }, function () {
            gulp.start("less");
        }
    );
});

gulp.task('watch_scripts', ['scripts'], function () {

    return watch(
        configHelper.getFullPathsOnEnvironment(GULP_SETTINGS.javascript.sources),
        { debounceDelay: 2000 }, function () {
            gulp.start("scripts");
        }
    );
});

gulp.task('watch', ['watch_scss', 'watch_less']);
gulp.task('service', function () {
    if (GULP_SETTINGS.scss.watcher === true) {
        gulp.start('watch_scss');
    } else {
        gulp.start('scss');
    }


    if (GULP_SETTINGS.less.watcher === true) {
        gulp.start('watch_less');
    } else {
        gulp.start('less');
    }

    if (GULP_SETTINGS.javascript.watcher === true) {
        gulp.start('watch_scripts');
    } else {
        gulp.start('scripts');
    }
})
gulp.task('default', ['service']);