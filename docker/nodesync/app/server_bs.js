var browserSync = require("browser-sync").create();
const bsProxy = process.env.APP_HOST ? process.env.APP_HOST : 'localhost';
// const proxy = require('http-proxy-middleware');
const port = process.env.APP_BS_PORT ? process.env.APP_BS_PORT : 4000;
const uiPort = process.env.APP_BS_UI_PORT ? process.env.APP_BS_UI_PORT : 4001;
const logLevel = process.env.APP_BS_LOG ? process.env.APP_BS_LOG : 'debug';
console.log("uiPort: ", uiPort);
let basePath = "";
if(process.env.NODE_ENV == "production") {
  basePath = "/sync_root/contao/";
} else {
  basePath = "../../../app/contao/";
  console.log("vasePath", basePath);
}

// /**
//  * Configure proxy middleware
//  */
// var jsonPlaceholderProxy = proxy('/*', {
//   target: bsProxy,
//   changeOrigin: true, // for vhosted sites, changes host header to match to target's host
//   logLevel: 'debug'
// })

let bs = {};
// console.log(process.env)
bs.start = function () {
  browserSync.init({
    ui: {
      port: parseInt(uiPort),
      weinre: {
        port: (parseInt(uiPort) + 1)
      }
    },
    files: [
      basePath+"files/**/*.js",
      basePath+"files/**/*.css",
      basePath+"files/**/*.sass",
      basePath+"files/**/*.scss",
      basePath+"files/**/*.less",
      basePath+"contao/templates/**/*.html5",
      basePath+"contao/templates/**/*.php",
      {
        match: ["wp-content/themes/**/*.php"],
        fn: function (event, file) {
          /** Custom event handler **/
        }
      }
    ],
    // "watchOptions": {
    //   "usePolling": true,
    //   "interval": 20
    // },
    "server": false,
    proxy: {
      target: bsProxy,
      cookies: { stripeDomain: false },
      // changeOrigin: true

    },
    port: port,
    "middleware": false, //[jsonPlaceholderProxy],
    "ghostMode": {
      "clicks": true,
      "scroll": true,
      "forms": {
        "submit": true,
        "inputs": true,
        "toggles": true
      }
    },
    "logLevel": logLevel,
    "logPrefix": "BS",
    "logConnections": false,
    "logFileChanges": true,
    "minify": true,
    "logPrefix": "BS",
    "logConnections": false,
    "logFileChanges": true,
    "logSnippet": true,
    "rewriteRules": false,
    "open": false,
    "browser": "default",
    "xip": false,
    "hostnameSuffix": false,
    "reloadOnRestart": false,
    "notify": true,
    "scrollProportionally": true,
    "scrollThrottle": 0,
    "reloadDelay": 0,
    "reloadDebounce": 0,
    "plugins": [],
    "injectChanges": true,
    "startPath": null,
    "minify": true,
    "host": null,
    "codeSync": true,
    "timestamps": true,
    "tagNames": {
      "less": "link",
      "scss": "link",
      "css": "link",
      "jpg": "img",
      "jpeg": "img",
      "png": "img",
      "svg": "img",
      "gif": "img",
      "js": "script"
    },


    "clientEvents": [
      "scroll",
      "input:text",
      "input:toggles",
      "form:submit",
      "form:reset",
      "click"
    ],
    "socket": {
      "path": "/browser-sync/socket.io",
      "clientPath": "/browser-sync",
      "namespace": "/browser-sync",
      "clients": {
        "heartbeatTimeout": 5000
      }
    },



    watchEvents: [
      'add',
      'change',
      'unlink',
      'addDir',
      'unlinkDir'
    ],
    snippetOptions: {
      // whitelist: ['/wp-admin/admin-ajax.php'],
      // blacklist: ['/contao/**', '/contao-manager.phar.php'],

    },

    notify: {
      styles: {
        position: 'fixed',
        zIndex: '9999',
        top: '0',
        left: '50%',
        height: '30px',
        lineHeight: '30px',
        padding: 0,
        width: '300px',
        marginLeft: '-150px',
        backgroundColor: '#f47c00',
        color: '#FFF',
        fontSize: '11px',
        opacity: '0.9',
        borderRadius: '0 0 4px 4px',
        boxShadow: "0 0 15px rgba(0,0,0,0.4)"
      }
    }
  });

}

module.exports = bs;
