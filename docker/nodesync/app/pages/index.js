import Layout from '../components/layout'

export default () => (
  <Layout title="Contao in Docker">
    
    <h1>Contao in Docker</h1>
    <p>Control panel for developers</p>
    <img height="150" src="https://contao.org/files/images/layout/dl-contao-active.svg" />
  </Layout>
)
