import Layout from '../../components/layout'
import Link from 'next/link'
import Head from 'next/head'
import { List } from 'antd';
const data = [
  'https://html5up.net/',
  'https://mobirise.com/',
  'https://themes.contao.org'
];

export default () => (
  <Layout title='HTML Templates'>
  <List
      header={<div>A list of Theme resources</div>}
      footer={<div>Use it as a starter for your Contao website</div>}
      bordered
      dataSource={data}
      renderItem={item => (<List.Item><Link href={item}><a target="_blank">{item}</a></Link></List.Item>)}
    />
  
  </Layout>
)
