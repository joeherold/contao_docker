import Layout from '../../components/layout';
import Link from 'next/link';

export default () => (
  <Layout title='Browser Sync'>
    <div>Browser Sync</div>
    <Link href="http://localhost:3001">
    <a target="_blank"><img height="150" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAYFBMVEX////yR0fyOzv5tLTxMzP96enyRUXxNzfyQkL6vr7zUVH81tb0a2v0aWn5u7vyTU34ra3yTk7xLy/yPz/0b2/xKyv81NT4q6v83t795ub2iYn+9fX4o6P2i4v/+vr0amos4r1VAAADNElEQVR4nO3dXXuaMBiAYWMpSN1Q6eyq2+z//5cDHM4PPkIwyZtcz3PEIff1JoQeVBYLIiIiIiIiIiK6bf3q+w7sVqhkf/R9ExYrNlmqVPnh+z5sdfYptXzxfSd2KnZnX6zC/744he36jFVYvF374hPm6tYXmzDf3fviEuabR19Mwvv9F5uw6FifMQn7fXEI8571GYuw+/kSj3B4fuELH8/3uITj8wtbOLb/QhfqzS9cob4vTOEUX4jCab7whF1/H8UknDq/0IS650OoQpP5hSScvv/CEh4M5xeOcLMy9QUifC3NgYEIE4QIpYcQofwQIpQfQoTyQ4hQfggRyg8hQvkhRCg/hAjlhxCh/BAirPr1+4/k/2GbLzyp5XvyXa5xvjDPqquVXON84cuyua6Ma9+Yzp4mFGt8olCo8anC2vhNmvHJQoFzfLpQ3BwtCCtjKWiOVoSi5mhJKMhoTVgbvyQYLQqFzNGqUMTZYVkoYI7Whd7PDgfCeo4//BmdCOs5elurjoQe96MzoTejQ6Eno1OhF6NjoQejc6Hzs8OD0PHZ4UXodI6ehPUcHRm9CZs5fsYtrIz7beRClSaxC1Vi/xdCESJEiBAhQoQIESJEiBAhQoQIESJEiBAhQoQIESJEiBAhQoQIESJEiBAhQoQIESJEiBAhQoQIESJEGJxw9nfXZglL+8LFau638+YIV8o+cPGZLud9/9BcmGa7gwNhdYsrU+MsYeXLnfgao+EcZwjT7M2drzG+mxiNhWm2cetrjGq60VDofH4X4+T9aCR0uv/u207cjwZCb/MzM04WevdNNU4UivDV6RsnCcX46raaZ8cEoZfzYSi9OWoLRc2vbbscN2oK00zJ89WNz1FLKHJ+bduRdwANodfzXafhOY4KRc+vbWiOI8LKV/i+fa36jYPCIObX1rdWB4RB+eq63wF6heLOd5261mqPUPzzs6fT4ztAp1Ds+a7T/X7sEAbz/OzpdLsfH4TV/gva13Q9xzth6PO79PPyzLkRVs+XOHx1rfFKGMX6vO58dpT194E+ysjm96/q7Ej2x+byuE9UdL6mdfvLlQcXP2FJRERERERERBRWfwF/dlfmJuz13QAAAABJRU5ErkJggg==" /></a>
    </Link>
    <div style={{width:"100%"}}>
    <iframe src="http://localhost:3001" width="100%" noborder></iframe> 
    </div>
  </Layout>
)
