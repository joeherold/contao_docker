let mkdirp = require("mkdirp");
let fs = require("fs");
let https = require('https');
let path = require("path");


const app = require('express')();
const bodyParser = require('body-parser');

// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(bodyParser.json({ type: 'application/json' }))

// parse some custom thing into a Buffer
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }))

// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }))

// parse an plain text body into a string
app.use(bodyParser.text({ type: 'text/plain' }))

const server = require('http').Server(app)
const io = require('socket.io')(server)
const next = require('next')
const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev })
const nextHandler = nextApp.getRequestHandler()

// var browserSync = require("browser-sync");
const APP_BS_AVAILABLE = process.env.APP_BS_AVAILABLE == "true" ? true : false;


// check folder structure
let contaoWebFolderPath = "/sync_root/contao/web";
let contaoFilesFolderPath = "/sync_root/contao/files";
if (process.env.NODE_ENV !== "production") {
  contaoWebFolderPath = "../../../app/contao/web";
  contaoFilesFolderPath = "../../../app/contao/files";
}
const managerBundleURI = 'https://download.contao.org/contao-manager/stable/contao-manager.phar';
const managerBundlePath = path.join(contaoWebFolderPath, "contao-manager.phar.php");

if (!fs.existsSync(contaoWebFolderPath)) {

  if(!fs.existsSync(contaoFilesFolderPath)) {
    mkdirp.sync(contaoFilesFolderPath); 
  }
  mkdirp.sync(contaoWebFolderPath);
  if (!fs.existsSync(managerBundlePath)) {
    var file = fs.createWriteStream(managerBundlePath);
    let request = https.get(managerBundleURI, function (response) {
      response.pipe(file);
    });
  }
}





// This file doesn't go through babel or webpack transformation.
// Make sure the syntax and sources this file requires are compatible with the current node version you are running
// See https://github.com/zeit/next.js/issues/1245 for discussions on Universal Webpack or universal Babel
const { createServer } = require('http')
const { parse } = require('url')



// fake DB
const messages = [];


if (APP_BS_AVAILABLE === true || 1 == 1) {
  var bs = require("./server_bs.js");
  bs.start();
}

// socket.io server
io.on('connection', socket => {
  socket.on('message', (data) => {
    messages.push(data)
    socket.broadcast.emit('message', data)
  })
})

nextApp.prepare().then(() => {
  app.get('/messages', (req, res) => {
    res.json(messages)
  })

  app.post('/messages/scss', (req, res)=>{
    console.log(req.body);
    io.sockets.emit("message", req.body);
    res.json(req.body);
  })
  app.get('*', (req, res) => {
    return nextHandler(req, res)
  })

  server.listen(8080, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:8080')
  })
})


// app.prepare().then(() => {
//   let srv = createServer((req, res) => {
//     // Be sure to pass `true` as the second argument to `url.parse`.
//     // This tells it to parse the query portion of the URL.
//     const parsedUrl = parse(req.url, true)
//     const { pathname, query } = parsedUrl

//     if (pathname === '/a') {
//       app.render(req, res, '/b', query)
//     } else if (pathname === '/b') {
//       app.render(req, res, '/a', query)
//     } else {
//       handle(req, res, parsedUrl)
//     }
//   });

//   var io = require('socket.io')(srv);

//   srv.listen(8080, err => {
//     if (err) throw err
//     console.log('> Ready on http://localhost:8080')
//   });

//   io.on('connection', function (socket) {
//     socket.emit('message', { hello: 'world' });
//     socket.on('my other event', function (data) {
//       console.log(data);
//     });
//   });


// })


