import { Component } from 'react';
import stylesheet from 'antd/dist/antd.min.css'
import Link from 'next/link'
import Head from 'next/head'
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import io from 'socket.io-client';

const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;


function notifyMe(message = {}, title="theTitle") {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var icon = 'https://www.freeiconspng.com/uploads/error-icon-15.png';
    if(message && message['status'] == "success") {
      icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACoCAMAAABt9SM9AAAAaVBMVEXMZpn////KXZTLY5fKX5XJWpLKYJX89vn9+fvw1eLv0uD57fPObJ3dnr3Pb5/hqcTkssr04OrRdqP25u7UgKnov9PmuM7XirDfo8Dcmbrak7by2uXqxNbWhq368fbrx9jjrsjSeqbHUY7zYb6SAAAI+0lEQVR4nO2cbXuiOhCGMZNEBEV5ERRFpf//R55k8gJWaHXP2dMKc3/Z7bb2kmdnJk8mE4OAIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAhi6gAAlwr150+/lV8NMCaDtqgP2+12UxUZME6CDQFcBHV5isNFxzLdV5yRXveoEKr2+WKQqBLyp9/f7wE4a87LYaWQvKboMgBb30ZiqmMVUHAFARdN9J1SWL0K/tNv9afhfPNtUDmaeccWh+1lSJYwX0XX6zWK8v7KGK5nXLek2D4W9WV0qxImBNMIAe3h1NUt9tNv+aeQ7PAgVbrNhOB3vl2Zr+Tqvl/NM7SAVfFnpQ7JsF0HUdlsjGYZWjz5tAKGZSvGNza8sDn6f77HXwKI7b1U8YF9bTpFaX6wmF0esvbeLSh7/p0pgMz8aD0zsYDdh1V8/FYqhTQV7jAvYwpwV62Wh+f2yHyOYvHizi/s2JNPz0zmHudk4tmxL1WUPG8FlrMzWuLWz8BaPP3o0JrXJPMRS+x7Wl35CynFjcr5fEwpO9+F1UsvNSWrnE19Z724StcvVWpZz8yTOhOu2T9frcxr83llIT90Wh1eSkEVk3ZdmItxkEWnVf1igLilMH5R47cF4j/Xyr22nklgCd+/Wxxf1Yql1sLOJLDcaqa4vfjIwG1beTmXDjzzSbh6VStwLfhqJkko/Uq4DF4LD544mbdzsQ3SB9bmJQsOwqdvOZOC1atYr7lKHvhlYT+XuAqEb/cdHurOeKRJsVnOLq4U/lj58Zn3I0WMs2PnzDbz0Qoq99Cnh4eGMB0ILmDrbSdV3M4mB1U+bXw2PTw1LxdxLWQvuoAzqKPeiMNezsRfIXz3xVooc1X2D2shGMfZhqDadbMNOhiLkRQEkFNUkfte8kDXAALMt3h1LsvzNf00U3NqRno5wLLquGmm1wsE7xw2AyYcgrFRtrhsx46pwR7TRtNTKxh3DhpRfx4RUeRl8cVQt2u6TnAGSaysAiN9AymafTe1FsarsgLxKahA1bPu39h0mzbeO4RjfgkYC9qmqqqmyZTFesw+qM+rNI9sDx7W9hdOL7BUIKRfFC0HWIa+J+14xMmoI+1RbTpFswqup3z5w8VeWDuxNWK7vJ7myRhzJzv7p2IBPjsoZl9uDqTd/JHTbmowN5H1fVMZWNBsqruV0BU927QQ7rD2q6x+Y2Dt7OZ3o/8sueqFMe43kVnUTztIFtOOLJU6zht8abuBu4Q9dSHoFz8znYWHH5fp1iyFbJ1aXxyFwdqtm4tFF1peQBySxMC64vL4akP/feCZy8Tt2DPKrLc37Eax4NIXC1uJCVaxy2TFUmHjqvx5eM8nW9MaTSvIe7tu7ls8Og2h0b9AmIZiT9BRj/amAHd75jQZqDZuESiZ1L6qdsOmQu1t8sh8xxjcMAHjvFKGv5bxdaH8f1Os+TPDvG+Cn/xbNo99QOvztRXTe7/sdsBQQd9wwxoVMdNJ3LGAG0/fgGTJIXI3hcP4eoTJXONkjas/u/tOlb/0pLcw6DrVl7n+DsaQ2QOEAGxp9gF2hUw/jt2SYAXbvzCp+ruRfrJ71TuqgK5Nk0FvPEk5dhQuEmYA9/ChF0bsNTBzTDZ4V3g3mRv7bGNzJs5sfQFR+fDANr07O4u5FaUB40tjrZw59nBzSJr8ur9tb+U1coqfJjMXwRMrTVhhfRbJyj92iA/ptoJnptJNSZsLd+oR2tgLvKLLcxUIxrjkuoW/rs/4XxFPRq3unpPaKbLuNiEmUNA7wVa+Crfgeg/oBHRDD25ipPy4kwUYw+IXT8fc88xarg3Yo5+dKT74XbcVXOhCbgu6t/Gp1cqZtsvAIXdkonIqALdddKNRlJiui+07h14XLPUYbpiPC9+lCZRWJ4yt7LGtirO+U5psFrXvuueFYCbA0LX7tVAVe21IQ/MC2xMzpl17slTgi4b6NHqvPaWNI3zY3vBlo1wkO3VhI9zSWAFWL3uObUd48eav7mgtA3MX7zZUnaSyc+v/83H+JsBaV5hw6oOjV41xdfRzzWCa93Zh46ZI6Zu/Upv6mptYG7xVpw3+VC5ysrWRKrQV2/bzsGT5ixipQN3saJa/96Nqkb58qNyWacU/1ixkJOTeDi7tyrYL7NNah7nXj8dcLSu5ViM0FZ1pn5WbuoZFrQHTik+HVz1V7B7HUN4PYLXZH64yJpamQtuzLf140t9IrHnrA4vrxkyJ2/Ba6sspObMj3yOLnip8E2g6SzvbEB7VThpruYonuwDqxGF+iCbTHt18pAokStXTx8mIpXO2/Ajwq7G5XHGZwEVO5xginGHDjoJa4+2gjYosTEiMvDDQouxMYKkidQmMF0vMEmm8xljTVVW7+N2z0HvRg2nQYIlOhYssFWN4GoH7wCUGFj6xjrYwMZYiZd2VqWU1Joj62Xe/FeXOIy6FfUiMLCUW9or1CmcCKzNK6MjBkq8VbuzGuQLvLa7rsfVOWfh33+7wzPb3fLMc7xFqsYx1CAMsS7vGhQ6mEi6Ele2jnoXt2lzKbHR6S5XFd59vdh8oE3U9UrkwNct9bMMqQsn8xK5u82GwXQ+m1F1NCSs2X01vQZ4Wbx5X7pi1N4BmZgJ1wrBen2YvWvs39FCsN2Pq1r6HYYhPvH0b3gZP3DuwMhLhAVfVKaLMgv1bi70Z3zk+D50JTRIXOz0TaQ8d8F+EH5XcM/eFiSOz+C1PB/n28fIsrmXen5Q0NzbNzJY/SQ39F65G83VRJPDs57BMgYHAklWvZvvpQJyVZ7dL3BtOmtZZ8xOEnRcwgP14GTcnagbijWMPuJjxBwS7WfjuQ/vA9qdyl5fAjvsym1GyjeKupOydGDKwadfb7ko+mcPRf4UrWXYTAqKNHws+YXBixUIVaykSNxV6+ek39hvxH3C0arL22N3WaSnvHunuHN5RUT0fwI+u9wlJq2HE40W5nHzCCI+hVU7xIup/BK/vpLpmb95x+ruw4mSbLcvT9v07Tn8ZYEFRHY9VEUxopPgvglfmKaYIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiBG+Adwg1mKbfpDHQAAAABJRU5ErkJggg==';
    } else {
      // icon = '';
    }
    var notification = new Notification(title, {
      // icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
      // icon: 'https://contao.org/files/images/layout/dl-contao-active.svg',
      icon: icon,
      body: message.message,
    });

    notification.onclick = function () {

      window.open("/css");      
    };

  }

}

class PageLayout extends Component {
  // fetch old messages data from the server
  // static async getInitialProps({ req }) {
  //   const response = await fetch('https://next-socket-io.now.sh/messages')
  //   const messages = await response.json()
  //   return { messages }
  // }

  // connect to WS server and listen event
  componentDidMount() {
    console.log("connectiong socket");
    this.socket = io('http://localhost:8080')
    this.socket.on('message', this.handleMessage);

    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
    }
  
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  }

  // close socket connection
  componentWillUnmount() {
    this.socket.off('message', this.handleMessage)
    this.socket.close()
  }

  static defaultProps = {
    messages: [],
  }

  // add messages from server to the state
  handleMessage = (message) => {
    console.log('message', message);
    notifyMe(message, status);
    this.setState(state => ({ messages: state.messages.concat(message) }))
  }


  // init state with the prefetched messages
  state = {
    field: '',
    messages: this.props.messages,
  }

  render() {
    return <Layout style={{ height: "100vh" }}>
      <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
      <Head>
        <title>{this.props.title}</title>
        <meta charSet='utf-8' />
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />
        <script src="/socket.io/socket.io.js"></script>
        {/* <script>
      var socket = io('http://localhost');
      socket.on('news', function (data) {
        console.log(data);
        socket.emit('my other event', { my: 'data' });
      });
    </script> */}
      </Head>
      <Header className="header">


        <Menu
          theme="dark"
          mode="horizontal"
          // defaultSelectedKeys={['0']}
          style={{ lineHeight: '64px' }}
        >
          <Menu.Item key="1"><Link href='/'><a>Home</a></Link></Menu.Item>

          <Menu.Item key="3"><Link href='/browsersync'><a>Browser Sync</a></Link></Menu.Item>
          <Menu.Item key="4"><Link href='/css'><a>LESS / SASS compiler</a></Link></Menu.Item>
          <Menu.Item key="5"><Link href='/ftp'><a>FTP</a></Link></Menu.Item>
          <Menu.Item key="6"><Link href='/manager'><a>Contao Manager</a></Link></Menu.Item>
          <Menu.Item key="7"><Link href='/templates'><a>Templates</a></Link></Menu.Item>
        </Menu>

      </Header>

      <Content style={{ padding: '0 50px', marginTop: 64 }}>

        <Breadcrumb style={{ margin: '16px 0' }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb>
        <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>{this.props.children}</div>
      </Content>

      <Footer style={{
        fontSize: 12,
        lineHeight: 1,
        verticalAlign: "center"
      }}>

        <Link href='https://contao.org'><a><img height="12" src="https://contao.org/files/images/layout/dl-contao-active.svg" /></a></Link> <strong>dev</strong> {' in '}
        <Link href='https://www.docker.com/'><a><strong>DOCKER</strong></a></Link>
        {/* {': Contao management console by '}<Link href='https://webpixels.at' target="_blank"><a>webpixels</a></Link> */}
      </Footer>
    </Layout>
  }

}
// export default ({ children, title = 'This is the default title' }) => (

// )


export default PageLayout;