# Docker commands

# create a new project with this boilerplate
```git clone https://bitbucket.org/joeherold/contao_docker.git <project-folder-name>```

## start contao container
Diesen Ordner in Visual Studio Code öffnen.
Dann kann über die Tasks der Container gestartet und gestoppt werden. (cmd + shift + b)

```docker-compose up```

## start contao container detached
```docker-compose up -d```

## stop contao container
```docker-compose stop```

## accessing the bash of a running container
```docker ps```
```docker exec -it <mycontainer> bash```

## backup the database
```docker-compose exec mysql usr/bin/mysqldump -u root --password=root contao > database/backups/contao.backup.sql```

## All docker variables
https://dockerfile.readthedocs.io/en/latest/content/DockerImages/dockerfiles/php-apache-dev.html


# CONTAO INSTALL
Use Cmd + Shift + P for all available build tasks

